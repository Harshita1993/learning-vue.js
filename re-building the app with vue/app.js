//Takes a JS object where we configure the vue app
Vue.createApp({
  //holds a function as a value, similar to js
  data(){
    return {
      goals: [],
      enteredValue: ''
    };
  },
  methods: {
    //we can define methods, functions that should be callable inside HTML
    addGoal() {
      this.goals.push(this.enteredValue);
      this.enteredValue = '';
    }
  }
}).mount('#app');